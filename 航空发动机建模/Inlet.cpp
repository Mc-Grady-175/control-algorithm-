#include<iostream>
#include<cstdio>
#include<math.h>

using namespace std;

int Inlet()
{
	double H, P0, T0, Ma;
	double T0s, P0s, C0, T1, P1, RC, T2, P2, R;
	double R = 8.3145;
	double k = 1.4, R = 287;
	//计算进气道进口静温和静压
	cin >> "进气道高度：" >> H >> end;
	cin >> "进气道马赫数：" >> H >> end;
	cin >> "进气道进口静压" >> P0 >> endl;
	cin >> "进气道进口静温" >> T0 >> endl;
	//cin >> H >> P0 >> T0 >> Ma;

	//计算进气道进口静温
	if (H < 0) { T0s = T0; }
	else if (H >= 0 && H <= 11000) { T0s = 288.15 - 0.0065 * H; }
	else if (H >= 11000 && H <= 24000) { T0s = 216.5; }
	//计算进气道进口静压
	if (H < 0) { P0s = P0; }
	else if (H >= 0 && H <= 11000) { P0s = 101325 * pow(1 - 0.225577 * 0.0001 * H, 5.25588); }
	else { P0s = 22632 * exp((11000 - H) / 6318); }
	//计算进口气流速度
	C0 = Ma * sqrt(k * R * T0s);
	//计算进口气流总温
	T1 = T0s * (1 + (k - 1) / 2 * Ma * Ma);
	///计算进口气流总压
	P1 = P0s * pow(1 + (k - 1) / 2 * Ma * Ma, k / (k - 1));
	//总压恢复系数经验公式
	if (Ma <= 1.0) { RC = 1; }
	else { RC = 1 - 0.075 * pow(Ma - 1, 1.35); }
	//出口气体总参
	T2 = T1;
	P2 = P1 * RC;
	cout << "进气道进口静温为：" << T0s << endl;
	cout << "进气道进口静压为：" << P0s << endl;
	cout << "进口气流速度为：" << C0 << endl;
	cout << "进口气流总温为：" << T1 << endl;
	cout << "总压恢复系数为：" << RC << endl;
	printf("出口气流总参数：T2=%.2lf P2=%.2lf", T2, P2);
	return 0;
}