#include<iostream>
#include<cstdio>

using namespace std;

int main()
{
	double NLcor, NH, T22, NHd, T22d, Wcor, Eff, WA22, Cm, P22, P0, T0, T22, EffC, Cn, Eff, T3, ΔT, T22, WB, Wi2, N, TB, Ti1, Ti2, T, NB, HB, Hi1, WBj, ND, Wi3, Hi2, Cl,CT, NC, h3, h22, WA22, WD3, WD5, ND, WA3, WA22, WHTCool, WLTCool，CHTCool, CHTCool，Hil;
	
	//高压相似转速
	NHcor = (NH / sqrt(T22)) / (NHd / sqrt(T22d));

	//压气机特性图插值进口流量与风扇效率
	//Wcor = f4(NHcor, P3 / P22);
	//Eff = f5(NHcor, P3 / P22);
	WA22 = Cm * Wcor * (P22 / P0) * sqrt(T0 / T22);
	EffC = Cn * Eff;

	//压气机出口总温
	T3 = (1 + (ΔT / T22) / EffC) * T22;

	//压气机放气模型
	WB = Wi2 * 0.01*N;
	TB = Ti1 + (Ti2 - Ti1) * ΔT / (0.01 * T);
	NB = WB * (HB - Hi1);
	double WB[n],lB[n];
	for (int i = 1; i <= n; i++)
	{
		Wi3 = Wi2 - WB[i];
	}
	for (int i = 1;i <= n; i++)
	{
		ND = Wi3 * (Hi2 - Hil) + lB[i] * (Cl + 1) / (CT + 1);
	}

	//压气机功模块
	NC = (h3 - h22) * (WA22 - WD3 - WD5) + ND;

	//压气机出口流量和冷却气流
	//高，低压涡轮冷却流量系数
	double WHTCool = CHTCool * WA22, WLCool = CLTCool * WA22;
	WA3 = WA22 - WHTCool - WLTCool;

	return 0;
}