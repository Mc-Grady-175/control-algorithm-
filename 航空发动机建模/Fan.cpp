#include<iostream>
#include<cstdio>

using namespace std;

int main()
{
	double NLcor, NL, T2, NLd, T2d, Wcor, Eff, WA2, Cm, W, P2, P0, T2, T0, EffF, Cn, T22, ΔT, NF, H21, H2, P21, P2, PiF, P22, SP, NH, WA2, WA21;

	//低压相似转速
	NLcor = (NL / sqrt(T2)) / (NLd / sqrt(T2d));

	//风扇特性图插值进口流量与风扇效率
	Wcor = f1(NLcor, P1 / P2);
	Eff = f2(NLcor, P1 / P2);
	WA2 = Cm * Wcor * (P2 / P0) * sqrt(T0 / T2);
	EffF = Cn * Fff;

	//风扇功和出口总温
	T22 = (1 + (ΔT / T2) / EffF) * T2;
	NF = WA2 * (H21 - H2);

	//风扇出口总压和分离器总压恢复系数SP
	P21 = P2 * PiF;
	P22 = P21 * SP;
	SP = f3(NH);

	//风扇出口流量
	WA21 = WA2;

	//量模块
	return 0;
}