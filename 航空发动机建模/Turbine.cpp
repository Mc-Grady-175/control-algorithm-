#include<iostream>
#include<cstdio>
#include<cmath>

int main()
{
	double Ncor, N, Tin, Nd, Tind, Wcor, Eff, Pi, WGin, Cm, Wcor, Pin, P0, T0, Tin, EffT, Cη, Eff, Tout, NT, Hout, Hin, k, f, Tavg, β, NT, WGout, WGin, WCool, P41, P4, PiHT, P5, P42, P55, P5,σLT, WG55, K, T55, A55, Ma55, P5S

	//涡轮相似转速
	Ncor = (N / sqrt(Tin)) / (Nd / sqrt(Tind));

	//涡轮特性插值计算进口流量与风扇效率
	Wcor = f8(Ncor, Pi);//插值法
	Eff = f9(Ncor, Pi);//插值法
	WGin = Cm * Wcor * (Pin / P0) * sqrt(T0 / Tin);
	EffT = Cη * Eff;

	//涡轮出口总温和涡轮功
	double k = fgas(f, Tavg), Tavg = Tin * β + Tout * (1 - β);
	Tout = Tin * [1 - (1 - pow(Pi, (1 - k) / k) * EffT];
	NT = W * Gin(Hout - Hin);

	//涡轮出口流量
	WGout = WGin + WCool
	//冷却气流与燃气混合后涡轮出口燃气温度
	Tout = (WGin * Tin + WCool * TCool) / WGout;

	//涡轮出口压力和总压损失系数
	P41 = P4 * PiHT;//高压涡轮出口压力
	P5 = P42 * PiHT;//低压涡轮出口压力
	P55 = P5 * σLT;//内涵喷管出口总压
	double K = sqrt(k55 * pow(2 / (k55 + 1), (k55 + 1) / (k55 - 1)) / R);
	WG55 = K * P55 * A55 * q(Ma55) / sqrt(T55);//内涵喷管出口流量
	q(Ma55) = Ma55 * pow(2 * (1 + (k55 - 1) / 2) * Ma55 * Ma55 / (k55 + 1), (-k55 - 1) / (2 * (k55 - 1)));//流量函数
	P5S = P55 / pow(1 + (1 - k55) * Ma55 * Ma55 / 2, k55 / (k55 - 1));

	return 0;
}